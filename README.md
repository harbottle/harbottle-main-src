# **harbottle-main-src**: SRPMS for Harbottle's main repo for Enterprise Linux

**Source RPMs can now be found alonside binary RPMs in the
[main repo.](https://harbottle.gitlab.io/harbottle-main/7/x86_64/)**
