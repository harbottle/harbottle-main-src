FROM centos:latest
MAINTAINER ergel <grainger@gmail.com>
RUN yum -y install http://harbottle.gitlab.io/harbottle-main-release/harbottle-main-release-7.rpm
RUN yum -y install epel-release
RUN yum -y install nodejs-bower
